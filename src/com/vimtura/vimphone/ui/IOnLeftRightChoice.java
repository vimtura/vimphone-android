package com.vimtura.vimphone.ui;

public interface IOnLeftRightChoice {

    /**
     * The interface was triggered because the user grabbed the left handle and
     * moved it past the target zone.
     */
    int LEFT_HANDLE = 0;

    /**
     * The interface was triggered because the user grabbed the right handle and
     * moved it past the target zone.
     */
    int RIGHT_HANDLE = 1;

    /**
     * Called when the user moves a handle beyond the target zone.
     * 
     * @param v The view that was triggered.
     * @param whichHandle Which "dial handle" the user grabbed, either
     *            {@link #LEFT_HANDLE}, {@link #RIGHT_HANDLE}.
     */
    void onLeftRightChoice(int whichHandle);

    
    public interface IOnLeftRightProvider {
        void setOnLeftRightListener(IOnLeftRightChoice l);
    }
}
