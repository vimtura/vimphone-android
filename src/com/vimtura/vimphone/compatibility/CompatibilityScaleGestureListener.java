package com.vimtura.vimphone.compatibility;


public interface CompatibilityScaleGestureListener {
	public boolean onScale(CompatibilityScaleGestureDetector detector);
}