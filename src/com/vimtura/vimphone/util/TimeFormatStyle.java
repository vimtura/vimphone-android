package com.vimtura.vimphone.util;

public enum TimeFormatStyle {
	DEFAULT("%d hrs, %d mins, %d secs", "%d mins, %d secs", "%d secs"),
	SHORT("%dh %dm %ds", "%dm %ds", "%ds");
	
	protected String hms;
	protected String ms;
	protected String s;
	
	TimeFormatStyle(String hms, String ms, String s) {
		this.hms = hms;
		this.ms = ms;
		this.s = s;
	}

	public String getHms() {
		return hms;
	}

	public String getMs() {
		return ms;
	}

	public String getS() {
		return s;
	}
}
