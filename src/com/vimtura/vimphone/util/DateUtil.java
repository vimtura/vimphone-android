package com.vimtura.vimphone.util;

public class DateUtil {
	public static String formatSeconds(int seconds) {
		return formatSeconds(seconds, TimeFormatStyle.DEFAULT);
	}
	
	public static String formatSeconds(int seconds, TimeFormatStyle style) {
		return formatMilliSeconds(seconds * 1000, style);	
	}	
	
	public static String formatMilliSeconds(long millis) {
		return formatMilliSeconds(millis, TimeFormatStyle.DEFAULT);
	}
	
	public static String formatMilliSeconds(long millis, TimeFormatStyle style) {
		int seconds = (int) (millis / 1000) % 60 ;
		int minutes = (int) ((millis / (1000*60)) % 60);
		int hours   = (int) ((millis / (1000*60*60)) % 24);
		
		if (hours > 0) {
			return String.format(style.getHms(), hours, minutes, seconds);
		} else if (minutes > 0) {
			return String.format(style.getMs(), minutes, seconds);
		} else {
			return String.format(style.getS(), seconds);
		}
	}
}
