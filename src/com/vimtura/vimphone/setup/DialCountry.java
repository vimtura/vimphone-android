package com.vimtura.vimphone.setup;

import com.vimtura.vimphone.R;

public enum DialCountry {
	NONE(R.string.pref_dialing_country_none, null, null),
	AU(R.string.pref_dialing_country_au, "61", "0011");
	
	protected int name;
	protected String prefix;
	protected String intPrefix;
	
	DialCountry(int name, String prefix, String intPrefix) {
		this.name = name;
		this.prefix = prefix;
		this.intPrefix = intPrefix;
	}

	public int getName() {
		return name;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getIntPrefix() {
		return intPrefix;
	}
}
