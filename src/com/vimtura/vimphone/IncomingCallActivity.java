/*
IncomingCallActivity.java
Copyright (C) 2011  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package com.vimtura.vimphone;

import java.util.List;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallParams;
import org.linphone.mediastream.Log;

import android.app.Activity;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.vimtura.vimphone.R;
import com.vimtura.vimphone.LinphoneSimpleListener.LinphoneOnCallStateChangedListener;
import com.vimtura.vimphone.compatibility.Compatibility;
import com.vimtura.vimphone.header.VimturaContact;
import com.vimtura.vimphone.header.VimturaHeaderFactory;
import com.vimtura.vimphone.header.VimturaHeader;
import com.vimtura.vimphone.header.VimturaQueue;
import com.vimtura.vimphone.ui.AvatarWithShadow;
import com.vimtura.vimphone.ui.InCallAnswerControls;
import com.vimtura.vimphone.ui.InCallAnswerControls.InCallAnswerControlTrigger;
import com.vimtura.vimphone.ui.LinphoneSliders;
import com.vimtura.vimphone.ui.LinphoneSliders.LinphoneSliderTriggered;
import com.vimtura.vimphone.ui.MarqueeTextView;

/**
 * Activity displayed when a call comes in.
 * It should bypass the screen lock mechanism.
 *
 * @author Guillaume Beraudo
 */
public class IncomingCallActivity extends Activity implements LinphoneOnCallStateChangedListener, InCallAnswerControlTrigger {

	private static IncomingCallActivity instance;
	
	private MarqueeTextView mNameView;
	private MarqueeTextView mNumberView;
	private MarqueeTextView mSingleView;	
	private ImageView mPictureView;
	private RelativeLayout mQueueView;
	private LinphoneCall mCall;
	private InCallAnswerControls mIncomingCallWidget;
	
	public static IncomingCallActivity instance() {
		return instance;
	}
	
	public static boolean isInstanciated() {
		return instance != null;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.incoming);

		mNameView = (MarqueeTextView) findViewById(R.id.incoming_caller_name);
		mNumberView = (MarqueeTextView) findViewById(R.id.incoming_caller_number);
		mSingleView = (MarqueeTextView) findViewById(R.id.incoming_caller_single);
		mPictureView = (ImageView) findViewById(R.id.incoming_picture);
		Compatibility.setViewAlpha(mPictureView, 0.50f);

		mQueueView = (RelativeLayout) findViewById(R.id.incoming_queue_bar);	
		Compatibility.setViewAlpha(mQueueView.getBackground(), 128);
		
        // set this flag so this activity will stay in front of the keyguard
        int flags = WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON;
        getWindow().addFlags(flags);

        // "Dial-to-answer" widget for incoming calls.
        mIncomingCallWidget = (InCallAnswerControls) findViewById(R.id.incoming_answer_controls);
        mIncomingCallWidget.setOnTriggerListener(this);
        mIncomingCallWidget.setCallLockerVisibility(View.VISIBLE);

        super.onCreate(savedInstanceState);
		instance = this;
	}

	@Override
	protected void onResume() {
		super.onResume();
		instance = this;
		
		// Avoid duplicates
		LinphoneManager.removeListener(this);
		LinphoneManager.addListener(this);	
		
		// Only one call ringing at a time is allowed
		if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
			List<LinphoneCall> calls = LinphoneUtils.getLinphoneCalls(LinphoneManager.getLc());
			for (LinphoneCall call : calls) {
				if (State.IncomingReceived == call.getState()) {
					mCall = call;
					break;
				}
			}
		}
		if (mCall == null) {
			Log.w("Couldn't find incoming call (sadly normal error for now, ignore)");
			finish();
			return;
		}
				
		VimturaHeader vimHeader = VimturaHeaderFactory.get(mCall);	
				
		// Contact or caller details
		VimturaContact vimContact = vimHeader.getContact();
		vimContact.fillOutView(this, mNumberView, mNameView, mSingleView, mPictureView);
		
		// Queue details
		VimturaQueue vimQueue = vimHeader.getQueue();		
		if (vimQueue != null) {
			vimQueue.fillOutView(this, mQueueView);
			mQueueView.setVisibility(View.VISIBLE);
		} else {
			mQueueView.setVisibility(View.GONE);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		LinphoneManager.removeListener(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		instance = null;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (LinphoneManager.isInstanciated() && (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME)) {
			LinphoneManager.getLc().terminateCall(mCall);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onCallStateChanged(LinphoneCall call, State state, String msg) {
		if (call == mCall && State.CallEnd == state) {
			finish();
		}
		if (state == State.StreamsRunning) {
			// The following should not be needed except some devices need it (e.g. Galaxy S).
			LinphoneManager.getLc().enableSpeaker(LinphoneManager.getLc().isSpeakerEnabled());
		}
	}

	private void decline() {
		LinphoneManager.getLc().terminateCall(mCall);
	}
	
	private void answer() {
		LinphoneCallParams params = LinphoneManager.getLc().createDefaultCallParameters();
		if (mCall != null && mCall.getRemoteParams() != null && mCall.getRemoteParams().getVideoEnabled() && LinphoneManager.isInstanciated() && LinphoneManager.getInstance().isAutoAcceptCamera()) {
			params.setVideoEnabled(true);
		} else {
			params.setVideoEnabled(false);
		}
		
		boolean isLowBandwidthConnection = !LinphoneUtils.isHightBandwidthConnection(this);
		if (isLowBandwidthConnection) {
			params.enableLowBandwidth(true);
			Log.d("Low bandwidth enabled in call params");
		}
		
		if (!LinphoneManager.getInstance().acceptCallWithParams(mCall, params)) {
			// the above method takes care of Samsung Galaxy S
			Toast.makeText(this, R.string.couldnt_accept_call, Toast.LENGTH_LONG).show();
		} else {
			if (!LinphoneActivity.isInstanciated()) {
				return;
			}
			final LinphoneCallParams remoteParams = mCall.getRemoteParams();
			if (remoteParams != null && remoteParams.getVideoEnabled() && LinphoneManager.getInstance().isAutoAcceptCamera()) {
				LinphoneActivity.instance().startVideoActivity(mCall);
			} else {
				LinphoneActivity.instance().startIncallActivity(mCall);
			}
		}
	}

	@Override
	public void onAnswerTrigger() {
		answer();
		finish();
	}

	@Override
	public void onRejectTrigger() {
		decline();
		finish();
	}
}
