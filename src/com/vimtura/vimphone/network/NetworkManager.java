/*
ContactPickerActivity.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package com.vimtura.vimphone.network;

import org.linphone.mediastream.Log;

import com.vimtura.vimphone.LinphoneManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

/**
 * 
 * Intercept network state changes and update linphone core through LinphoneManager.
 *
 */
public class NetworkManager extends BroadcastReceiver {

	protected static NetworkState activeNetworkState;
	
	public static NetworkState getActiveNetworkState() {
		return activeNetworkState;
	}
	
	public static void initActiveNetworkState(ConnectivityManager connectivityManager) {
		activeNetworkState = new NetworkState(connectivityManager);
	}

	@Override
	public void onReceive(Context context, Intent intent) {		
		NetworkState newState = new NetworkState(context, intent);
		
		boolean stateChanged = false;		
		if (activeNetworkState == null) {
			stateChanged = true;
		} else {
			if (!activeNetworkState.equals(newState)) {
				stateChanged = true;
			}
		}
		
		if (stateChanged) {
			Log.i("Network State Changed [was: " + activeNetworkState + ", now: " + newState + "]");
		}
		
		// Update active state regardless
		activeNetworkState = newState;		
		
		if (stateChanged) {
			if (LinphoneManager.isInstanciated()) {
				LinphoneManager.getInstance().connectivityChanged(newState);
			}			
		}
	}

}